#!/usr/bin/env bash
#
# The purpose of this script is to demonstrate how to preview a file or an
# image in the preview window of fzf.
#
# Dependencies:
# - https://github.com/sharkdp/bat
# - https://github.com/hpjansson/chafa

if [[ $# -ne 1 ]]; then
	>&2 echo "usage: $0 FILENAME"
	exit 1
fi

file=${1/#\~\//$HOME/}
type=$(file --dereference --mime -- "$file")

if [[ ! $type =~ image/ ]]; then
	if [[ $type =~ =binary ]]; then
		file "$1"
		exit
	fi

	# Sometimes bat is installed as batcat.
	if command -v bat >/dev/null; then
		bat --style="${BAT_STYLE:-numbers}" --color=always --pager=never -- "$file"
	else
		cat "$1"
	fi

	exit
fi

dim=${FZF_PREVIEW_COLUMNS}x${FZF_PREVIEW_LINES}
if [[ $dim = x ]]; then
	dim=$(stty size </dev/tty | awk '{print $2 "x" $1}')
elif ((FZF_PREVIEW_TOP + FZF_PREVIEW_LINES == $(stty size </dev/tty | awk '{print $1}'))); then
	# Avoid scrolling issue when the Sixel image touches the bottom of the screen
	# * https://github.com/junegunn/fzf/issues/2544
	dim=${FZF_PREVIEW_COLUMNS}x$((FZF_PREVIEW_LINES - 1))
fi

# 1. Use kitty icat on kitty terminal
if command -v chafa >/dev/null; then
	chafa -f sixel -s "$dim" "$file"
	# Add a new line character so that fzf can display multiple images in the preview window
	echo

# 4. Cannot find any suitable method to preview the image
else
	file "$file"
fi
