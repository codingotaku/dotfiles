#!bin/sh
# /etc/skel/.bash_profile

# This file is sourced by bash for login shells.  The following line
# runs your .bashrc and is recommended by the bash info pages.
if [[ -f ~/.bashrc ]]; then
    . ~/.bashrc
fi

export XDG_CONFIG_HOME="${HOME}/.config/"
export XDG_DATA_HOME="${HOME}/.local/share/"
export XDG_STATE_HOME="${HOME}/.local/state/"

# Allow multiple bash sessions to write to history at the same time
shopt -s histappend

if test -z "${XDG_RUNTIME_DIR}"; then
    export XDG_RUNTIME_DIR=$(mktemp -d /tmp/$(id -u)-runtime-dir.XXX)
    chmod 0700 "${XDG_RUNTIME_DIR}"
fi

if [ -z "${WAYLAND_DISPLAY}" ] && [ "$(tty)" == "/dev/tty1" ]; then
    #dbus-launch --sh-syntax --exit-with-session sway
    dbus-run-session sway
fi
