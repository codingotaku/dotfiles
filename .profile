####
# Wayland support start
####

# Session
export XDG_SESSION_TYPE=wayland
export XDG_SESSION_DESKTOP=sway
export XDG_CURRENT_DESKTOP=sway

# Firefox
export MOZ_ENABLE_WAYLAND=1
export MOZ_DBUS_REMOTE=1

# QT
export QT_QPA_PLATFORM=wayland
export QT_QPA_PLATFORMTHEME=qt5ct
export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"

# GTK
export GTK_THEME='Dracula'

# For games which can support wayland through sdl
export LD_PRELOAD=/usr/lib64/libSDL2.so
export SDL_DYNAMIC_API=/usr/lib64/libSDL2.so
export SDL2_DYNAMIC_API=/usr/lib64/libSDL2.so.0
export SDL3_DYNAMIC_API=/usr/lib64/libSDL3.so.0
export SDL_VIDEODRIVER=wayland
export SDL_VIDEO_GL_DRIVER=libOpenGL.so

# Java
export _JAVA_AWT_WM_NONREPARENTING=1
export STUDIO_JDK=/usr/lib/jvm/java-17-openjdk
export JDK_JAVA_OPTIONS="-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dswing.crossplatformlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel"

# Others
export GDK_BACKEND=wayland
export GTK_USE_PORTAL=0 # https://github.com/swaywm/sway/issues/5732
export CLUTTER_BACKEND=gdk
export GST_GL_WINDOW=wayland # https://gstreamer.freedesktop.org/documentation/gstreamer/running.html

####
# Wayland support END
####

####
# Terminal hacks start
####

# FZF bindings
export FZF_CTRL_T_OPTS="--preview '~/.fzf-scripts/fzf-preview.sh {}'"
export FZF_CTRL_R_OPTS="--preview 'echo {}' --preview-window down:3:hidden:wrap --bind '?:toggle-preview'"
export FZF_ALT_C_OPTS="--preview 'ls -1 --color=always {} | head -200'"
source /usr/share/fzf/key-bindings.bash
export FZF_DEFAULT_OPTS='--color=fg:#f8f8f2,bg:#282a36,hl:#bd93f9 --color=fg+:#f8f8f2,bg+:#44475a,hl+:#bd93f9 --color=info:#ffb86c,prompt:#50fa7b,pointer:#ff79c6 --color=marker:#ff79c6,spinner:#ffb86c,header:#6272a4'

# Set default editor and terminal
export TERMINAL=/sbin/foot
export ALTERNATE_EDITOR=""
export EDITOR="emacsclient -t"          # $EDITOR opens in terminal
export VISUAL="emacsclient -c -a emacs" # $VISUAL opens in GUI mode

# Use terminal for GPG signing, avoids GTK2 dependency for the dialog
#export GPG_TTY=$(tty)

# remove all but the last identical command, and commands that start with a space:
export HISTCONTROL="erasedups:ignorespace"

# Bindings to make bash more interactive
bind 'set show-all-if-ambiguous on'
bind '"\e[Z":menu-complete-backward'
shopt -s autocd

## Use the up and down arrow keys or Ctrl+p and Ctrl+n for finding a command in history
## (you can write some initial letters of the command first).
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'
bind '"\C-p":history-search-backward'
bind '"\C-n":history-search-forward'

# Add local bins and dev bin folders to the path
export PATH="$PATH:$HOME/.local/bin"

# For the conig alias in .bash_aliases
source /usr/share/bash-completion/completions/git
__git_complete config __git_main
