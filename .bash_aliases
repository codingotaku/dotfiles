# Make things colorful
alias ls='ls --color=yes'
alias diff='diff --color=auto'
alias grep='grep --color=auto'

# Shortcuts
alias l='ls -CF'
alias ll='ls -alF'
alias e='$EDITOR'
alias ccb="echo 'Clearing clipboard' && wl-copy -c"
alias cbh="echo 'Fully clearing bash history...' && cat /dev/null > ~/.bash_history && history -c && history -w"

# ignore foot terminfo on ssh
alias ssh='env TERM=xterm-256color ssh'

# Dot file config
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

