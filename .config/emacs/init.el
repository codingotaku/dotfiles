;; DO NOT EDIT THIS FILE DIRECTLY
;; This is a file generated from a literate programing source file located at
;; https://codeberg.org/codingotaku/dotfiles/src/branch/main/.config/emacs/init.org
;; You should make any changes there and regenerate it from Emacs org-mode using C-c C-v t

;; Performance tweaks for modern machines
(setq gc-cons-threshold (* 100 1024 1024)) ; 100MB during startup
(add-hook 'emacs-startup-hook
        (lambda ()
          (setq gc-cons-threshold (* 2 1000 1000)) ; 2MB after startup
          (message "Emacs ready in %s with %d garbage collections."
                   (format "%.2f seconds"
                           (float-time
                            (time-subtract after-init-time before-init-time)))
                   gcs-done)))
(setq read-process-output-max (* 1024 1024)) ; 1mb

(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

(global-set-key [C-backspace] 'backward-kill-word)

;; Better buffer navigation
(global-set-key [C-tab] 'next-buffer)
(global-set-key [M-tab] 'previous-buffer)

(defalias 'yes-or-no-p 'y-or-n-p)

(require 'package)

(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(unless (package-installed-p 'vc-use-package)
  (package-vc-install "https://github.com/slotThe/vc-use-package"))
(require 'vc-use-package)

(use-package visual-fill-column
:ensure t
:config
(progn
  (setq-default visual-fill-column-center-text t)
  (setq-default visual-fill-column-width 80)
  (add-hook 'visual-line-mode-hook #'visual-fill-column-mode))
)

(add-hook 'org-mode-hook 'visual-line-mode)

;; Remove extra UI clutter by hiding the scrollbar, menubar, and toolbar.
(menu-bar-mode -1) ;; Hide menubar

(when (display-graphic-p) 
  (tool-bar-mode -1) ;; Hide the toolbar if using the emacs GUI, this is not needed for the terminal version
  )
(setq inhibit-splash-screen t) ;; remove the splash screen
(setq inhibit-startup-screen t) ;; remove the welcome screen

(setq warning-minimum-level :error)

(use-package dracula-theme
  :ensure t
  :config
  (load-theme 'dracula t))

;; Set the font. Note: height = px * 100
(set-face-attribute 'default nil :font "Fira Code" :height 140)

(use-package all-the-icons
  :ensure t)

(add-to-list 'default-frame-alist '(alpha-background . 90))

(fido-vertical-mode)

(use-package marginalia
  :ensure t
  ;; Bind `marginalia-cycle' locally in the minibuffer.  To make the binding
  ;; available in the *Completions* buffer, add it to the
  ;; `completion-list-mode-map'.
  :bind (:map minibuffer-local-map
              ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode))

(setq tramp-default-method "ssh")

(require 'uniquify)
(setq uniquify-buffer-name-style 'forward
      window-resize-pixelwise t
      frame-resize-pixelwise t
      load-prefer-newer t)
;; Fast minibuffer selection
(fido-vertical-mode)

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

(setq backup-by-copying t
      backup-directory-alist `(("." . ,(concat user-emacs-directory "backups")))
      custom-file (expand-file-name "custom.el" user-emacs-directory))

;; Better dired, i use this a lot, check their repository to know more
;; 
(use-package dirvish
  :ensure t
  :init
  (dirvish-override-dired-mode)
  :config
  (dirvish-peek-mode)              ; Preview files in minibuffer
  (dirvish-side-follow-mode)       ; similar to `treemacs-follow-mode'
					;(setq dirvish-side-auto-expand t)
 (setq dirvish-open-with-programs
  `((,dirvish-audio-exts . ("mpv" "%f"))
    (,dirvish-video-exts . ("mpv" "%f"))
    (,dirvish-image-exts . ("swayimg" "%f"))
    ))
  (setq dirvish-reuse-session nil)
  (setq dirvish-mode-line-format
	'(:left (sort symlink) :right (omit yank index)))
  (setq dirvish-attributes
	'(file-time file-size collapse subtree-state vc-state git-msg all-the-icons))
  (setq dirvish-default-layout '(0 0.4 0.6))
  (push "rm" dirvish-video-exts)
  (setq dired-listing-switches
	"-l --almost-all --human-readable --group-directories-first --no-group")
  :bind      ; Bind `dirvish|dirvish-side|dirvish-dwim' as you see fit
  (          ; ("C-c f" . dirvish-fd-ask)
   ("C-o" . dirvish-side)
   :map dirvish-mode-map           ; Dirvish inherits `dired-mode-map'
   ("a"   . dirvish-quick-access)
   ("f"   . dirvish-file-info-menu)
   ("y"   . dirvish-yank-menu)
   ("Y"   . dired-do-copy)
   ("N"   . dirvish-narrow)
   ("^"   . dirvish-history-last)
   ("h"   . dirvish-history-jump)       ; remapped `describe-mode'
   ("s"   . dirvish-quicksort)  ; remapped `dired-sort-toggle-or-edit'
   ("v"   . dirvish-vc-menu)    ; remapped `dired-view-file'
   ("TAB" . dirvish-subtree-toggle)
   ("M-f" . dirvish-history-go-forward)
   ("M-b" . dirvish-history-go-backward)
   ("M-l" . dirvish-ls-switches-menu)
   ("M-m" . dirvish-mark-menu)
   ("M-t" . dirvish-layout-toggle)
   ("M-s" . dirvish-setup-menu)
   ("M-e" . dirvish-emerge-menu)
   ("M-j" . dirvish-fd-jump)))

;; Automatically insert closing parens
(electric-pair-mode t)

;; make return key also do indent, globally
(electric-indent-mode 1)

;; Visualize matching parens
(show-paren-mode 1)

;; Prefer spaces to tabs
;; (setq-default indent-tabs-mode nil)

;; Automatically save your place in files
(save-place-mode t)

;; Save history in minibuffer to keep recent commands easily accessible
(savehist-mode t)

;; Keep track of open files
(recentf-mode t)

;; Keep files up-to-date when they change outside Emacs
(global-auto-revert-mode t)

;; Display line numbers only when in programming modes
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

(use-package ts-fold
  :vc (:fetcher github :repo emacs-tree-sitter/ts-fold))

(require 'treesit)
(setq treesit-simple-indent-rule 2)
(setq major-mode-remap-alist
      '((css-mode  . css-ts-mode)
        (python-mode . python-ts-mode)
        (java-mode . java-ts-mode)
        (bash-mode . bash-ts-mode)
        (js-mode . js-ts-mode)
        (js2-mode . js-ts-mode)
        (php-mode . php-ts-mode)
        (typescript-mode . typescript-ts-mode)
        (rust-mode . rust-ts-mode)
        (json-mode . json-ts-mode)))

(use-package treesit-auto
  :ensure t
  :demand t
  :config
  (global-treesit-auto-mode))

(use-package php-mode
  :ensure t)

(use-package jtsx
  :ensure t
  :mode (("\\.jsx?\\'" . jtsx-jsx-mode)
	 ("\\.tsx\\'" . jtsx-tsx-mode)
	 ("\\.ts\\'" . jtsx-typescript-mode))
  :commands jtsx-install-treesit-language
  :hook ((jtsx-jsx-mode . hs-minor-mode)
	 (jtsx-tsx-mode . hs-minor-mode)
	 (jtsx-typescript-mode . hs-minor-mode)))

(use-package rust-mode
  :ensure t
  :hook
  ((rust-mode-hook . eglot-ensure))
  )

(use-package lua-mode
  :ensure t)

(use-package dockerfile-mode
  :ensure t)

(use-package docker-compose-mode
  :ensure t)

(use-package markdown-mode
  :ensure t
  :mode ("README\\.md\\'" . gfm-mode)
  ;; These extra modes help clean up the Markdown editing experience.
  ;; `visual-line-mode' turns on word wrap and helps editing commands
  ;; work with paragraphs of text.
  :hook ((markdown-mode . visual-line-mode))
  :init (setq markdown-command "pandoc")
  :bind (:map markdown-mode-map
	      ("C-c C-e" . markdown-do)))

(use-package edit-indirect
  :ensure t)

;; TypeScript, JS, and JSX/TSX support.
(use-package web-mode
  :ensure t
  :mode (("\\.ts\\'" . web-mode)
	 ("\\.js\\'" . web-mode)
	 ("\\.hbs\\'" . web-mode)
	 ("\\.mjs\\'" . web-mode)
	 ("\\.tsx\\'" . web-mode)
	 ("\\.html?\\'" . mhtml-mode)
	 ("\\.jsx\\'" . web-mode))
  :hook
  ((web-mode-hook . (lambda () (electric-pair-local-mode -1))))
  :custom
  (web-mode-content-types-alist '(("jsx" . "\\.js[x]?\\'") ("tsx" . "\\.ts[x]?\\'")))
  (web-mode-code-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-markup-indent-offset 2)
  (web-mode-enable-current-column-highlight t)
  (web-mode-enable-auto-pairing t)
  (electric-pair-mode nil)
  (web-mode-enable-part-face t)
  (web-mode-enable-comment-interpolation t)
  (web-mode-enable-css-colorization t)
  (web-mode-enable-auto-quoting t))

(use-package yaml-mode
  :ensure t)

(use-package csv-mode
  :ensure t)

(use-package projectile
  :ensure t
  :init
  (projectile-mode +1)
  :config
  (setq projectile-term "vterm")
  (add-to-list 'projectile-ignored-projects "~/")
  (setq projectile-project-root-files '(".git" "composer.json" "package.json" "Makefile" "setup.py"))
  ;; The prject lookup is hard coded to the ones I use, you will want to replace this
  (setq projectile-project-search-path '(("~/code" . 2)))
  :bind (:map projectile-mode-map
              ("C-c p" . projectile-command-map)))

(defun projectile-ignore-homedir (dir)
  (if (and dir (equal dir (expand-file-name "~/")))
    nil
  dir))

(advice-add 'projectile-locate-dominating-file :filter-return #'projectile-ignore-homedir)

(defun project-ignore-homedir (project)
  (if (and project
    (equal (expand-file-name (nth 2 project)) (expand-file-name "~/")))
       nil
      project))
(advice-add 'project-try-vc :filter-return #'project-ignore-homedir)

(use-package eglot
  :ensure t
  :bind (:map eglot-mode-map (("C-c ." . eglot-code-action-quickfix)))
  ;; Add your programming modes here to automatically start Eglot,
  ;; assuming you have the respective LSP server installed.
  :hook ((web-mode . eglot-ensure)
         ((html-mode mhtml-mode) . eglot-ensure)
         ((css-mode css-ts-mode) . eglot-ensure)
         (jsx-ts-mode . eglot-ensure)
         (tsx-ts-mode . eglot-ensure)
         (typescript-ts-mode . eglot-ensure)
         (rust-ts-mode . eglot-ensure)
         (python-mode . eglot-ensure)
         (go-mode . eglot-ensure)
         (php-mode . eglot-ensure)
         (lua-mode . eglot-ensure))
  :config
  ;; You can configure additional LSP servers by modifying
  ;; `eglot-server-programs'. The following tells eglot to use an alternative server for the specified modes
  (add-to-list 'eglot-server-programs '((html-mode mhtml-mode) . ("vscode-html-language-server" "--stdio")))
  (add-to-list 'eglot-server-programs '((php-mode phps-mode) . ("intelephense" "--stdio")))
  (add-to-list 'eglot-server-programs '((css-mode css-ts-mode) . ("vscode-css-language-server" "--stdio")))
  (add-to-list 'eglot-server-programs
         `((js-mode js-ts-mode tsx-ts-mode typescript-ts-mode typescript-mode)
           . ("typescript-language-server" "--stdio")))
  (add-to-list 'eglot-server-programs '((rust-ts-mode rust-mode) . ("rust-analyzer" :initializationOptions (:check (:command "clippy")))))
  (add-to-list 'eglot-server-programs '((python-ts-mode python-mode) . ("pylsp"))))

(use-package simple-httpd
  :ensure t)

(use-package corfu
  :ensure t
  :init
  (global-corfu-mode)
  :custom
  (with-eval-after-load 'corfu
    (add-to-list 'corfu-backend 'company-capf))
  (completion-styles '(basic)))

(use-package corfu-terminal
  :ensure t
  :config
  (unless (display-graphic-p)
    (corfu-terminal-mode +1)))

(use-package cape
:ensure t
;; Bind prefix keymap providing all Cape commands under a mnemonic key.
;; Press C-c p ? to for help.
:bind ("C-c p" . cape-prefix-map) ;; Alternative keys: M-p, M-+, ...
:init
;; Add to the global default value of `completion-at-point-functions' which is
;; used by `completion-at-point'.  The order of the functions matters, the
;; first function returning a result wins.  Note that the list of buffer-local
;; completion functions takes precedence over the global list.
(add-hook 'completion-at-point-functions #'cape-dabbrev)
(add-to-list 'completion-at-point-functions #'cape-file)
(add-to-list 'completion-at-point-functions #'cape-keyword)
;; ...
)

(use-package emacs
  :init
  ;; TAB cycle if there are only few candidates
  ;; (setq completion-cycle-threshold 3)

  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (setq tab-always-indent 'complete)

  ;; Emacs 30 and newer: Disable Ispell completion function. As an alternative,
  ;; try `cape-dict'.
  (setq text-mode-ispell-word-completion nil)

  (setq read-extended-command-predicate #'command-completion-default-include-p))

;; Helpful package to support debug adapter protocol, similar to dap-mode for lsp-mode
;; Read more about it on https://github.com/svaante/dape
(use-package dape
  :ensure t
  :preface
  ;; By default dape shares the same keybinding prefix as `gud'
  ;; If you do not want to use any prefix, set it to nil.
  ;; (setq dape-key-prefix "\C-x\C-a")
  :bind (:map dape-active-mode (("M-n" . dape-next)
         ("M-i" . dape-step-in)
         ("M-o" . dape-step-out)
         ("M-C" . dape-continue)))
  :hook
  ;; Save breakpoints on quit
  ((kill-emacs . dape-breakpoint-save)
   ;; Load breakpoints on startup
   (after-init . dape-breakpoint-load))

  :init
  ;; To use window configuration like gud (gdb-mi)
  (setq dape-buffer-window-arrangement 'gud)

  :config
  ;; Info buffers to the right
  (setq dape-buffer-window-arrangement 'right)

  ;; Global bindings for setting breakpoints with mouse
  (dape-breakpoint-global-mode)

  ;; Pulse source line (performance hit)
  (add-hook 'dape-display-source-hook 'pulse-momentary-highlight-one-line)

  ;; To not display info and/or buffers on startup
  ;; (remove-hook 'dape-start-hook 'dape-info)
  ;; (remove-hook 'dape-start-hook 'dape-repl)

  ;; To display info and/or repl buffers on stopped
  ;; (add-hook 'dape-stopped-hook 'dape-info)
  ;; (add-hook 'dape-stopped-hook 'dape-repl)

  ;; Kill compile buffer on build success
  ;; (add-hook 'dape-compile-hook 'kill-buffer)

  ;; Save buffers on startup, useful for interpreted languages
  ;; (add-hook 'dape-start-hook (lambda () (save-some-buffers t t)))

  ;; Projectile users
  (setq dape-cwd-fn 'projectile-project-root))

;; Add extra context to Emacs documentation to help make it easier to
;; search and understand. This configuration uses the keybindings
;; recommended by the package author.
(use-package helpful
  :ensure t
  :bind (("C-h f" . #'helpful-callable)
         ("C-h v" . #'helpful-variable)
         ("C-h k" . #'helpful-key)
         ("C-c C-d" . #'helpful-at-point)
         ("C-h F" . #'helpful-function)
         ("C-h C" . #'helpful-command)))


;; https://www.gnu.org/software/emacs/manual/html_node/emacs/EDE.html
(global-ede-mode 1)

;; Highlight lines you are in
(require 'hl-line)
;; let's enable it for all programming major modes
(add-hook 'prog-mode-hook #'hl-line-mode)
;; and for all modes derived from text-mode
(add-hook 'text-mode-hook #'hl-line-mode)

;; (use-package golden-ratio
;;   :ensure t
;;   :config
;;   (golden-ratio-mode 1))

;; mark changed lines in VCE tracked repositories
(use-package diff-hl
  :ensure t
  :after magit
  :init
  (global-diff-hl-mode)
  :config
  (diff-hl-margin-mode 1)
  (diff-hl-flydiff-mode 1)
  :hook (
         (prog-mode-hook . diff-hl-flydiff-mode)
         (magit-pre-refresh . diff-hl-magit-pre-refresh)
         (magit-post-refresh . diff-hl-magit-post-refresh))
  )

;; Flycheck suppoty of eglot to enable syntax checking
;; flycheck-eglot https://github.com/flycheck/flycheck-eglot
;; flycheck https://www.flycheck.org/en/latest/
(use-package flycheck-eglot
  :ensure t
  :after (flycheck eglot)
  :config
  (global-flycheck-eglot-mode 1))

;; An extremely feature-rich git client. Activate it with "C-c g".
(use-package magit
  :ensure t
  :bind (("C-c g" . magit-status)))

;; Breadcrumb adds, well, breadcrumbs to the top of your open buffers
;; and works great with project.el, the Emacs project manager.
;;
;; Read more about projects here:
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Projects.html
(use-package breadcrumb
  :vc (:fetcher github :repo joaotavora/breadcrumb)
  :init (breadcrumb-mode))

(use-package format-all
  :ensure t
  :hook ((format-all-mode-hook . format-all-ensure-formatter)  (prog-mode . format-all-mode))
  :bind (("C-c C-p" . 'format-all-region-or-buffer))
  :config
  (setq-default format-all-formatters
                '(("Shell" (shfmt "-i" "4" "-ci"))
                  ("Typescript" (shfmt "-i" "4" "-ci"))
                  ("Python" yapf))))

(use-package pet
  :ensure t
  :config
  ;; Emacs 29+
  ;; This will turn on `pet-mode' on `python-mode' and `python-ts-mode'
  :hook ((python-base-mode-hook . pet-mode)))

(use-package paredit
  :ensure t
  :hook ((emacs-lisp-mode . enable-paredit-mode)
         (lisp-mode . enable-paredit-mode)
         (ielm-mode . enable-paredit-mode)
         (lisp-interaction-mode . enable-paredit-mode)
         (scheme-mode . enable-paredit-mode)))

(use-package titlecase
  :ensure t
  :bind (("C-c t" . titlecase-region)))

(use-package auctex
  :ensure t)

(setq tab-width 4 ;; or any other preferred value
	c-basic-offset tab-width
	cperl-indent-level tab-width)

(setq css-indent-offset 2)

(use-package ligature
  :ensure t
  :config
  ;; Enable the "www" ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))
  ;; Enable traditional ligature support in eww-mode, if the
  ;; `variable-pitch' face supports it
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  ;; Enable all Cascadia Code ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
                                       ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                       "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
                                       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
                                       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
                                       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                       "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
                                       ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
                                       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                       "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                       "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
                                       "\\\\" "://"))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode t))

(use-package vterm
  :ensure t
  :commands vterm
  :config
  (setq vterm-max-scrollback 10000))

;; credit: yorickvP on Github
(defvar wl-copy-process)
(setq wl-copy-process nil)

(defun wl-copy (text)
  "Use wl-copy to copy a given TEXT."
  (setq wl-copy-process (let ((default-directory "~"))
                          (make-process :name "wl-copy"
                                        :buffer nil
                                        :command '("wl-copy" "-f" "-n")
                                        :noquery t
                                        :connection-type 'pipe)))

  (process-send-string wl-copy-process text)
  (process-send-eof wl-copy-process))

(defun wl-paste ()
  "Use wl-copy to paste text from clipboard."
  (if (and wl-copy-process (process-live-p wl-copy-process))
      nil
    (let ((default-directory "~"))
      (shell-command-to-string "wl-paste -n | tr -d '\r'"))))

(setq interprogram-cut-function 'wl-copy)
(setq interprogram-paste-function 'wl-paste)

(if (fboundp 'pinentry-start)
    (pinentry-start 'quiet))

(defun my-clear ()
  "Use clear shell and recreate the prompt"
  (interactive)
  (let ((comint-buffer-maximum-size 0))
    (comint-truncate-buffer)))

(defun my-shell-hook ()
  "Use clear shell on pressing Ctrl+l"
  (local-set-key "C-l" 'my-clear))

(add-hook 'shell-mode-hook 'my-shell-hook)
(put 'erase-buffer 'disabled nil)

(defun insert-ucode (ucode)
  "Insert UCODE wrapped in hidden span at the current cursor position."
  (interactive "sEnter unicode to insert: ")  ; Prompt for input
  (insert (format "<span class=\"ucode\" aria-hidden=\"true\">%s </span>" ucode)))  ; Insert the formatted text

(use-package xkcd
  :ensure t
  :bind (:map xkcd-mode-map (("C-f" . xkcd-next)
	 ("C-b" . xkcd-prev))))
