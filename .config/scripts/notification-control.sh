#!/bin/env sh

status="${1:-enable}"

[[ $status = 'enable' ]] && makoctl mode -r do-not-disturb && notify-send 'DND mode disabled'
[[ $status = 'dnd' ]] && makoctl mode -a do-not-disturb
