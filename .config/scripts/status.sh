#!/usr/bin/env sh

separator='<span foreground="#bd93f9">\ue216</span>'

wrapper() {
    for i in "$@"; do
        [[ -n $i ]] && printf '%b %s ' "$separator" "$i"
    done
}

update_status() {
    while true; do
        battery_status=$("$HOME/.config/scripts/status-scripts/battery-monitor.sh")
        brightness_status=$("$HOME/.config/scripts/status-scripts/brightness-monitor.sh")
        wg_status=$("$HOME/.config/scripts/status-scripts/wg-status.sh")
        wifi_status=$("$HOME/.config/scripts/status-scripts/wifi-status.sh")
        volume=$("$HOME/.config/scripts/status-scripts/volume.sh")
        scratch_pad=$("$HOME/.config/scripts/status-scripts/scratch-pad-monitor.sh")
        date_formatted=$(printf '%b %s' '\uf273' "$(date '+%a %F %H:%M:%S')")

        bar=$(wrapper "${scratch_pad}" "${wg_status}" "${wifi_status}" "${volume}" "${battery_status}" "${brightness_status}" "${date_formatted}")

        printf '%s %b' "$bar" "$separator "
        sleep 1
    done
}

update_status
