#!/bin/env sh

Help() {
    # Display Help
    echo "This is a helper script to take screenshots in sway window manager"
    echo
    echo "Syntax: $(basename "$0") [-h] [-c | -p path] -t type"
    echo
    echo "options:"
    printf '%s\t\t%s\n' "-h" "Print this Help."
    printf '%s\t%s\n' "-c" "Copy to clipboard (requires wl-clipboard)."
    printf '%s\t%s\n' "-p path" "Path to place the screenshot to. Ignored when used with -c."
    printf '%s\t%s\n' "-t type" "Type of screenshot, this should be one of the following"
    printf '%s\t%s\n' "selected" "Select the aria to screenshot using slurp"
    printf '%s\t%s\n' "active" "Only for sway, select the active window."
    printf '%s\t%s\n' "whole" "Screenshot everything"
    printf '%s\t%s\n' "swappy" "Open swappy with selected aria. Both -c and -t are ignored when selecting swappy as the type."
    echo
}

types="selected active whole swappy"

while getopts 'hct:p:' option; do
    case "$option" in
        t)
            type="$OPTARG"
            echo "$types" | grep -w -q "$type" || (Help && exit)
            ;;
        c)
            copy=true
            ;;
        p)
            path="$OPTARG"
            ;;
        h | [?])
            Help
            exit
            ;;
        *)
            echo 'Error in command line parsing' >&2
            exit 1
            ;;
    esac
done
shift $((OPTIND - 1))

case "$type" in
    swappy)
        grim -g "$(slurp)" - | swappy -f -
        ;;
    selected)
        test -n "$copy" && grim -g "$(slurp)" - | wl-copy && notify-send -e "Screenshot copied to clipboard" && exit
        test -n "$path" && grim -g "$(slurp)" "$path" && notify-send -e "Screenshot taken" && exit
        grim -g "$(slurp)" "$HOME/ps_$(date +"%Y%m%d%H%M%S").png" && notify-send -e "Screenshot taken"
        ;;
    active)
        test -n "$copy" && swaymsg -t get_tree | jq -r '.. | select(.focused?) | .rect | "\(.x),\(.y) \(.width)x\(.height)"' | grim - | wl-copy && notify-send -e "Screenshot copied to clipboard" && exit
        test -n "$path" && swaymsg -t get_tree | jq -r '.. | select(.focused?) | .rect | "\(.x),\(.y) \(.width)x\(.height)"' | grim -g - "$path" && notify-send -e "Screenshot taken" && exit
        swaymsg -t get_tree | jq -r '.. | select(.focused?) | .rect | "\(.x),\(.y) \(.width)x\(.height)"' | grim -g - "$HOME/ps_$(date +"%Y%m%d%H%M%S").png" && notify-send -e "Screenshot taken"
        ;;
    whole)
        test -n "$copy" && grim - | wl-copy && notify-send -e "Screenshot copied to clipboard" && exit
        test -n "$path" && grim "$path" && notify-send -e "Screenshot taken" && exit
        grim "$HOME/ps_$(date +"%Y%m%d%H%M%S").png" && notify-send -e "Screenshot taken"
        ;;
esac
