#!/usr/bin/env bash

DOT_VARS="/tmp/dot-files/.vars"
BAT_FILE="$DOT_VARS/bat"
BAT_STATE="$DOT_VARS/bat_state"
BAT_ALERT_ID="$DOT_VARS/bat_alert_id"

read -r -n 1 prev_val <"$BAT_FILE"
read -r -n 1 prev_state <"$BAT_STATE"
read -r bat_alert_id <"$BAT_ALERT_ID"

bat_state=$(cat '/sys/class/power_supply/BAT0/status')
bat_percentage=$(cat '/sys/class/power_supply/BAT0/capacity')

# Nerd icons from https://www.nerdfonts.com/cheat-sheet
declare -A charging_icons=([10]='󰂅' [9]='󰂋' [8]='󰂊' [7]='󰢞' [6]='󰂉' [5]='󰢝' [4]='󰂈' [3]='󰂇' [2]='󰂆' [1]='󰢜' [0]='󰢟')
declare -A discharging_icons=([10]='󰁹' [9]='󰂂' [8]='󰂁' [7]='󰂀' [6]='󰁿' [5]='󰁾' [4]='󰁽' [3]='󰁼' [2]='󰁻' [1]='󰁺' [0]='󰂎')

# Colors from dracula theme color palette: https://draculatheme.com/contribute
declare -A bat_bg=([5]='#ff5555' [4]='#ff5555' [3]='#ff79c6' [2]='#ff79c6' [1]='#ffb86c' [0]='#44475a')

get_bat_icon_charging() {
    local key=$(("$bat_percentage" / 10))
    printf '%s' "${charging_icons[$key]}"
}

get_bat_icon() {
    if [[ $bat_state = 'Charging' ]]; then
        printf '%s' "$(get_bat_icon_charging)"
    elif [[ $bat_state = 'Not charging' ]]; then
        printf '󰂏'
    else
        local key=$(("$bat_percentage" / 10))
        printf '%s' "${discharging_icons[$key]}"
    fi
}

show_bat_notification() {
    # Displays notification while trying to replace the previous one.

    # $1 = Notification Urgency
    # $2 = Battery percentage
    # $3 = Additional message

    local id
    id=$(notify-send --replace-id="$bat_alert_id" --print-id --app-name 'System' --urgency "$1" "System Message $(get_bat_icon) " "Battery level is at ${bat_percentage}%. $2")

    echo "$id" >"$BAT_ALERT_ID"
}

# Show battery percentage alert if needed.
check_battery_status() {
    if [[ $bat_percentage -le 2 ]] && [[ $prev_val -lt 5 ]]; then
        echo 5 >"$BAT_FILE"
        show_bat_notification "critical" "System will power off soon!"
    elif [[ $bat_percentage -le 5 ]] && [[ $prev_val -lt 4 ]]; then
        echo 4 >"$BAT_FILE"
        show_bat_notification "critical" "Please plug-in your system!"
    elif [[ $bat_percentage -le 10 ]] && [[ $prev_val -lt 3 ]]; then
        echo 3 >"$BAT_FILE"
        show_bat_notification "critical" "Critical!"
    elif [[ $bat_percentage -le 20 ]] && [[ $prev_val -lt 2 ]]; then
        echo 2 >"$BAT_FILE"
        show_bat_notification "normal" "Very low!"
    elif [[ $bat_percentage -le 30 ]] && [[ $prev_val -lt 1 ]]; then
        echo 1 >"$BAT_FILE"
        show_bat_notification "normal"
    elif [[ $bat_percentage -gt 30 ]] && [[ $prev_val -ne 0 ]]; then
        echo 0 >"$BAT_FILE"
    fi
}

if [[ "$bat_state" = 'Not charging' ]] && [[ $prev_state -ne 2 ]]; then
    echo 2 >"$BAT_STATE"
    echo 0 >"$BAT_FILE"
    show_bat_notification "normal" "The battery is not charging!"
elif [[ $bat_state = 'Discharging' ]] && [[ $prev_state -ne 1 ]]; then
    echo 1 >"$BAT_STATE"
    show_bat_notification "$([[ $prev_val -gt 0 ]] && echo 'critical' || echo 'low')" "Unplugged"
elif [[ $bat_state = 'Charging' ]] && [[ $prev_state -ne 0 ]]; then
    echo 0 >"$BAT_STATE"
    echo 0 >"$BAT_FILE"
    show_bat_notification "low" "Plugged-in and Charging"
fi

[[ $bat_state = 'Discharging' ]] && check_battery_status

get_bat_color() {
    [[ $bat_state = 'Charging' ]] && echo '#50fa7b' || echo "${bat_bg[$prev_val]}"
}

get_bat_fg() {
    if [[ $bat_state = 'Charging' ]] || [[ $prev_val -ge 1 ]]; then
        printf '#282a36'
    elif [[ $prev_val -eq 0 ]]; then
        printf '#f8f8f2'
    fi
}

echo "<span background=\"$(get_bat_color)\" foreground=\"$(get_bat_fg)\"> $(get_bat_icon)  $bat_state $bat_percentage% " '</span>'
