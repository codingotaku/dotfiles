#!/bin/env sh

interfaces=$(wg show interfaces | tr -d '\n')

test -n "${interfaces}" && printf '[WG]  %s' "${interfaces}"
