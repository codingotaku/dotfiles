#!/bin/env sh

length=$(swaymsg -t get_tree | jq '.nodes[].nodes[] |select(.name == "__i3_scratch") | .floating_nodes | length')

test "$length" -gt 0 && printf '󱂬  %s' "$length"
