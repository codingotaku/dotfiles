#!/usr/bin/env bash

read -r wifi_signal < <(iw dev wlan0 link | grep 'signal' | awk '{print $2}')

[ -z "$wifi_signal" ] && echo "󰤭  No Wi-Fi" && exit 0

declare -A wifi_icons=([4]='󰤨' [3]='󰤥' [2]='󰤢' [1]='󰤟')

# Returns strength in stars based on dBm
get_strength() {
    if [[ "$1" -gt "-50" ]]; then
        printf '*****'
    elif [[ "$1" -gt "-67" ]]; then
        printf '****'
    elif [[ "$1" -gt "-70" ]]; then
        printf '***'
    elif [[ "$1" -gt "-80" ]]; then
        printf '**'
    else
        printf '*'
    fi
}

get_icon() {
    local key=$(("${#strength}" - 1))
    echo "${wifi_icons[$key]}"
}

read -r dev < <(tail -n1 /proc/net/dev | cut -d: -f1 2>/dev/null)

read -r ssid < <(iwctl station "$dev" show | grep 'Connected network' | awk '{$1=$2=""; print $0 }')

read -r strength < <(get_strength "$wifi_signal")

printf '%s   Wi-Fi %s' "$(get_icon)" "${strength}"
#printf '%s   %s %s' "$(get_icon)" "$ssid" "$strength"
