#!/usr/bin/env bash

declare -A brightness_icons=([5]='󰃚' [4]='󰃠' [3]='󰃝' [2]='󰃟' [1]='󰃝' [0]='󰃞')

brightness_percentage=$(brightnessctl --machine-readable | cut -d, -f4)

get_icon() {
    local brightness_val=${brightness_percentage::-1}
    local key=$(("$brightness_val" / 20))
    printf '%s' "${brightness_icons[$key]}"
}

printf '%s  Brightness %s' "$(get_icon)" "$brightness_percentage"
