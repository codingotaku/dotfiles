#!/bin/sh

echo
echo '###############################'
echo '##                           ##'
echo '##    Clone git repository   ##'
echo '##                           ##'
echo '###############################'
echo

CWD=$(pwd)
cd $HOME

# Remove existing .dotfiles directory to avoid any conflicts
rm -rf $HOME/.dotfiles

if [ "$1" != "ssh" ]; then
	# Use https instead of ssh
	git clone --bare https://codeberg.org/codingotaku/dotfiles.git $HOME/.dotfiles
else
	git clone --bare git@codeberg.org:codingotaku/dotfiles.git $HOME/.dotfiles
fi

echo
echo '###############################'
echo '##                           ##'
echo '##    Configure dotfiles     ##'
echo '##                           ##'
echo '###############################'
echo

config() {
	/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME $@
}

backup() {
	mkdir -p $HOME/.config-backup/$(dirname $1)
	echo "created $HOME/.config-backup/$(dirname $1)"
	mv $HOME/$1 $HOME/.config-backup/$1
	echo "Move $HOME/$1 to $HOME/.config-backup/$1"
}

export -f backup

config checkout
if [ $? = 0 ]; then
	echo "Checked out config."
else
	echo "Backing up pre-existing dot files."
	config checkout 2>&1 | egrep "^\s+[\.,a-z,A-Z]" | awk {'print $1'} | xargs -I{} bash -c 'backup "$@"' _ {}
fi

config checkout
config config status.showUntrackedFiles no

echo 'Configure neovim plugins and updates'
nvim --headless +PlugInstall +PlugUpdate +PlugUpgrade +qall

cd $CWD

echo
echo '##################################################'
echo '##                                              ##'
echo '##           Configuration Complete!            ##'
echo '##                                              ##'
echo '##################################################'
echo
